﻿using System;
using System.IO;
using System.Linq;

namespace Verzeichnisbaum
{
    class Program
    {
        static void Main(string[] args)
        {   
            //Erweiterung
            File.Create(@"c:\temp\beispiel\verzeichnisbaum.txt");
            FileInfo savePlace = new FileInfo(@"c:\temp\beispiel\verzeichnisbaum.txt");
            
            //Kerncode
            String path = new String(@"C:\Windows\assembly\");
            Program.findSubdirectory(path, savePlace);            
        }

        public static void findSubdirectory(string path, FileInfo savePlace)
        { try
            {  
                DirectoryInfo baseDir = new DirectoryInfo(path);
                string[] subDirectories = Directory.GetDirectories(path); 
                Console.Write("\t");
                //Erweiterung
                using (StreamWriter streamWrite = savePlace.AppendText())
                {
                    streamWrite.Write("\t");
                }

                //Kerncode
                foreach (string dir in subDirectories)
                {
                    if (dir.Any())
                    { Program.findSubdirectory(dir, savePlace); }
                    long totalSize = 0;
                    DirectoryInfo thisDir = new DirectoryInfo(dir);
                    try
                    {
                        FileInfo[] files = thisDir.GetFiles();
                        foreach (FileInfo file in files)
                        {
                            totalSize += file.Length;
                        }
                    }
                    catch (Exception e) { }
                    
                    Console.WriteLine(dir + "\t Size:" + totalSize);

                    //Erweiterung
                    using (StreamWriter stream = savePlace.AppendText())
                    {
                        stream.Write(dir + "\t Size:" + totalSize + "\n");
                    }
                }
            }catch (Exception e) { }
            
        }

    }
}
